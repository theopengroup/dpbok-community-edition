
[[KLP-cloud]]
==== Cloud Services

*Description*

(((virtualization, and managed services)))(((virtualization, and cloud)))(((mainframe, time-sharing)))Companies have always sought alternatives to owning their own computers. There is a long tradition of managed services, where applications are built out by a customer and then their management is outsourced to a third party. Using fractions of mainframe “time-sharing” systems is a practice that dates back decades. However, such relationships took effort to set up and manage, and might even require bringing physical tapes to the third party (sometimes called a “service bureau”). Fixed-price commitments were usually high (the customer had to guarantee to spend X dollars). Such relationships left much to be desired in terms of responsiveness to change.

As computers became cheaper, companies increasingly acquired their own ((data center))s, investing large amounts of capital in high-technology spaces with extensive power and cooling infrastructure. This was the trend through the late 1980s to about 2010, when cloud computing started to provide a realistic alternative with true “pay as you go” pricing, analogous to electric metering.(((cloud computing)))((("Parkhill, Douglas")))(((cloud computing, origins of)))

The idea of running IT completely as a utility service goes back at least to 1965 and the publication of _The Challenge of the Computer Utility_, by Douglas Parkhill (see <<fig-parkhill-300-o>>). While the conceptual idea of cloud and utility computing was foreseeable 50 years ago, it took many years of hard-won IT evolution to support the vision. Reliable hardware of exponentially increasing performance, robust open-source software, Internet backbones of massive speed and capacity, and many other factors converged towards this end.

[[fig-parkhill-300-o]]
.Initial Statement of Cloud Computing
image::images/1_02-parkhill.png[book, 275,, float="right"]

However, people store data — often private — on computers. In order to deliver compute as a utility, it is essential to segregate each customer's workload from all others. This is called _multi-tenancy_. In ((multi-tenancy)), multiple customers share physical resources that provide the illusion of being dedicated.

NOTE: The phone system has been multi-tenant ever since they got rid of https://en.wikipedia.org/wiki/Party_line_(telephony)[party lines]. A ((party line)) was a shared line where anyone on it could hear every other person.

In order to run compute as a utility, multi-tenancy was essential. This is different from electricity (but similar to the phone system). As noted elsewhere, one watt of electric power is like any other and there is less concern for information leakage or unexpected interactions. People's bank balances are not encoded somehow into the power generation and distribution infrastructure.

(((cloud computing, distinguished from virtualization)))(((cloud, private)))Virtualization is necessary, but not sufficient for cloud. True cloud services are highly automated, and most cloud analysts will insist that if virtual machines cannot be created and configured in a completely automated fashion, the service is not true cloud. This is currently where many in-house “private” cloud efforts struggle; they may have virtualization, but struggle to make it fully self-service.

[[KLP-cloud-models]]
(((cloud computing, Infrastructure as a Service)))(((cloud computing, Platform as a Service)))(((cloud computing, Software as a Service)))Cloud services have refined into at least three major models:

* Software as a Service (SaaS)
* Platform as a Service (PaaS)
* Infrastructure as a Service (IaaS)

****
*From the http://csrc.nist.gov/publications/nistpubs/800-145/SP800-145.pdf[NIST Definition of Cloud Computing (p.2-3)]:*

*Software as a Service (SaaS)* The capability provided to the consumer is to use the provider’s applications running on a cloud infrastructure. The applications are accessible from various client devices through either a thin client interface, such as a web browser (e.g., web-based email), or a program interface. The consumer does not manage or control the underlying cloud infrastructure including network, servers, OSs, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings.

*Platform as a Service (PaaS)* The capability provided to the consumer is to deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages, libraries, services, and tools supported by the provider. The consumer does not manage or control the underlying cloud infrastructure including network, servers, OSs, or storage, but has control over the deployed applications and possibly configuration settings for the application-hosting environment.

*Infrastructure as a Service (IaaS)* The capability provided to the consumer is to provision processing, storage, networks, and other fundamental computing resources where the consumer is able to deploy and run arbitrary software, which can include OSs and applications. The consumer does not manage or control the underlying cloud infrastructure but has control over OSs, storage, and deployed applications; and possibly limited control of select networking components (e.g., host firewalls) cite:[NIST2011].
****

There are cloud services beyond those listed above (e.g., Storage as a Service). Various platform services have become extensive on providers such as Amazon(TM), which offers load balancing, development pipelines, various kinds of storage, and much more.

*Evidence of Notability*

Cloud computing is one of the most economically active sectors in IT.
//The Amazon AWS(TM) re:Invent conference attracted 50,000 attendees in 2018.
Cloud computing has attracted attention from the US National Institute for Standards and Technology (NIST) cite:[NIST2011]. Cloud law is becoming more well defined cite:[Millard2013].

//Consider adding more citations, draw from MNSCU report?

*Limitations*

The future of cloud computing appears assured, but computing and digital competencies also extend to edge devices and in-house computing. The extent to which organizations will retain in-house computing is a topic of industry debate.

*Related Topics*

* xref:KLP-app-deliv[Application Development]
* xref:KLP-ops-mgmt[Operations Management]
* xref:KLP-sourcing[Sourcing and Vendor Management]
