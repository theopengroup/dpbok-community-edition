
[[digital-context]]
==== Digital Context

*Description*

[[KLP-digital-svc-position]]
===== Positioning Digital Products

Digital services can be:

* Directly market and consumer-facing (e.g., Facebook(R), LinkedIn(R)), to be used by external consumers and paid for by either them or closely associated customers (e.g., Netflix(R), or an online banking system)
* Customer “supporting” systems, such as the online system that a bank teller uses when interacting with a customer; customers do not interact directly with such systems, but customer-facing representatives do, and problems with such systems may be readily apparent to the end customer
* Completely “back-office” systems (human resources, payroll, marketing, etc.)

Note, however, that (especially in the current digitally transforming market) a service previously thought of as “back office” (when run internally) becomes “market-facing” when developed as a profit-seeking offering. For example, a human resources system built internally is “back office”, but Workday is a directly market-facing product, even though the two services may be similar in functionality.

In positioning a digital offering, one must consider the likelihood of its being adopted. Is it part of a broader “movement” of technological innovation? Where is the customer base in terms of its willingness to adopt the innovation? A well-known approach is the idea of "((diffusion theory))”, first researched by Everett Rogers and proposed in his _Diffusion of Innovations,_ cite:[Rogers2003].

Rogers' research proposed the idea of “Adopter Categorization on the Basis of Innovativeness”, with a well-known graphic (see <<fig-diffusion-graph-500-c>>, similar to cite:[Rogers2003] Figure 7-3, p.281).

[[fig-diffusion-graph-500-c]]
.Technology Adoption Categories (Rogers)
image::images/1_01-adoption.png[diffusion-graph, 500, ,float="right"]

Rogers went on to characterize the various stages:

* Innovators: venturesome risk-takers
* Early adopters: opinion leaders
* Early majority: deliberative, numerous
* Late majority: skeptical, also numerous
* Laggards: traditional, isolated, conservative

Steve Blank, in _The Four Steps to Epiphany_ cite:[Blank2013], argues there are four categories for startups (p.31):

* Startups that are entering an existing market
* Startups that are creating an entirely new market
* Startups that want to re-segment an existing market as a low-cost entrant
* Startups that want to re-segment an existing market as a niche player

Understanding which category you are attempting is critical, because “the four types of startups have very different rates of customer adoption and acceptance”.

(((Treacy and Wiersma)))Another related and well-known categorization of competitive strategies (((competitive strategy))) comes from Michael Treacy and Fred Wiersma cite:[Treacy1997]:

* Customer intimacy (((customer intimacy)))
* Product leadership (((product leadership)))
* Operational excellence (((operational excellence)))

It is not difficult to categorize well-known brands in this way:

.Companies and their Competitive Strategies
[cols="3*", options="header"]
|====
|Customer Intimacy|Product Leadership|Operational Excellence
|((Nordstrom))|((Apple))|((Dell Technologies))
|((Home Depot))|((Nike))|((Wal-Mart))
|====

However, deciding which strategy to pursue as a startup may require some experimentation.

[[KLP-consumer-customer-sponsor]]
===== Defining Consumer, Customer, and Sponsor

In understanding ((IT value)), (((digital value))) it is essential to clarify the definitions of ((user)), ((customer)), and ((sponsor)), and understand their perspectives and motivations. Sometimes, the user _is_ the customer. But more often, the user and the customer are different, and the role of system or service sponsor may additionally need to be distinguished. (((digital stakeholders)))

The following definitions may help:

* The *consumer* (sometimes called the *user*) is the person actually interacting with the IT or digital service
* The *customer* is a source of revenue for the service
** If the service is part of a profit center, the customer is the person actually purchasing the product (e.g., demand deposit banking). If the service is part of a cost center (e.g., a human resources system), the customer is best seen as an internal executive, as the actual revenue-producing customers are too far removed.
* The *sponsor* is the person who authorizes and controls the funding used to construct and operate the service

Depending on the service type, these roles can be filled by the same or different people. Here are some examples:

ifdef::backend-pdf[<<<]
.Defining Consumer, Customer, and Sponsor
[cols="5*", options="header"]
|====
|Example |Consumer |Customer |Sponsor |Notes
|Online banking 2+^|Bank account holder | Managing Director, consumer banking | Customer-facing profit center with critical digital component
|Online restaurant reservation application |Restaurant customers |Restaurant owners |Product owner | Profit-making digital product
|Enterprise human resources application |Human resources analyst 2+^|Vice-president, human resources | Cost center funded through corporate profits
|Online video streaming service |End video consumer (e.g., any family member) | Streaming account holder (e.g., parent) |Streaming video product owner | Profit-making digital product
|Social traffic application | Driver | Advertiser, data consumer | Product owner | Profit-making digital product
|====

So, who paid for the user's enjoyment? The bank and restaurant both had clear motivation for supporting a better online experience, and people now expect that service organizations provide this. The bank experiences less customer turnover and increased likelihood that customers add additional services. The restaurant sees increased traffic and smoother flow from more efficient reservations. Both see increased competitiveness.

The traffic application is a somewhat different story. While it is an engineering marvel, there is still some question as to how to fund it long term. It requires a large user base to operate, and yet end consumers of the service are unlikely to pay for it. At this writing, the service draws on advertising dollars from businesses wishing to advertise to passersby, and also sells its real-time data on traffic patterns to a variety of customers, such as developers considering investments along given routes.

This last example illustrates the maxim (attributed to media theorist and writer Douglas Rushkoff cite:[Solon2011]) that “if you don't know how the product is making money, you are the product”. (((user, as product)))(((banking)))

*Evidence of Notability*

The context for the existence and operation of a digital system is fundamental to its existence; the digital system in fact typically operates as part of a larger sociotechnical system. The cybernetics literature cite:[Wiener1948, Beer2002] provides theoretical grounding. The IT Service Management (ITSM) literature is also concerned with the context for IT services, in terms of the desired outcomes they provide to end consumers cite:[TSO2011].

ifdef::backend-pdf[<<<]
*Limitations*

Understanding a product context is important; however, there is feedback between a product and its context, so no amount of initial analysis will be able to accurately predict the ultimate outcome of fielding a given digital product (internally or externally). A concrete example is the concept of a network effect, in which a product becomes more valuable due to the size of its user base cite:[Griffin2018].

*Related Topics*

* xref:KLP-CA-product-mgmt[Product Management]
* xref:chap-invest-mgmt[Portfolio and Investment Management]
* xref:gov-chap[Governance]
* xref:chap-arch-portfolio[Enterprise Architecture]
