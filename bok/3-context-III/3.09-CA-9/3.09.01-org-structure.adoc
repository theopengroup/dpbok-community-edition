
[[KLP-organization]]
==== Structuring the Organization: Product and Function

*Description*

There are two major models that Digital Practitioners may encounter in their career:

* The traditional centralized back-office “IT” organization
* Digital technology as a component of market-facing product management

(((IT, history of)))The traditional IT organization started decades ago, with “back-office” goals like replacing file clerks and filing cabinets. At that time, computers were not flexible or reliable, business did not move as fast, and there was a lot of value to be gained in relatively simple efforts like converting massive paper filing systems into digital systems. As these kinds of efforts grew and became critical operational dependencies for companies, the role of Chief Information Officer (CIO) was created, to head up increasingly large organizations of application developers, infrastructure engineers, and operations staff.

The business objectives for such organizations centered on stability and efficiency. Replacing 300 file clerks with a system that didn't work, or that wound up costing _more_ was obviously not a good business outcome! On the other hand, it was generally accepted that designing and implementing these systems would take some time. They were complex, and many times the problem had never been solved before. New systems might take years — including delays — to come online, and while executives might be unhappy, oftentimes the competition wasn't doing much better. CIOs were conditioned to be risk-averse; if systems were running, changing them was scrutinized with great care and rarely rushed.

The culture and practices of the modern IT organization became more established, and while it retained a reputation for being slow, expensive, and inflexible, no-one seemed to have any better ideas. It didn't hurt that the end customer wasn't interested in computers.

(((Amazon)))Then along came the personal computer, and the ((dot-com boom)). Suddenly everyone had personal computers at home and was on the Internet. Buying things! Computers continued to become more reliable and powerful as well. Companies realized that their back-office IT organizations were not able to move fast enough to keep up with the new e-commerce challenge, and in many cases organized their Internet team _outside_ of the CIO's control (which sometimes made the traditional IT organization very unhappy). ((Silicon Valley)) startups such as ((Google)) and ((Facebook)) in general did not even have a separate “CIO” organization, because for them (and this is a critical point) *the digital systems _were_ the product*. Going to market against tough competitors (((Alta Vista))) (Alta Vista and ((Yahoo(R))) against Google, ((Friendster)) and ((MySpace)) against Facebook) wasn't a question of maximizing efficiency. It was about product innovation and effectiveness and taking appropriate risks in the quest for these rapidly growing new markets.

[[trad-cio-org]]
Let's go back to our example of the traditional CIO organization. A typical structure under the CIO might look as shown in <<fig-classic-org-600-c>>.

[[fig-classic-org-600-c]]
.Classic IT Organization
image::images/3_09-classic-org.png[org chart,600]

(We had some related discussion in xref:tbl-i-o-matrix[]). [[process-function]]Such a structure was perceived to be “efficient” because all the server engineers would be in one organization, while all the Java developers would be in another, and their utilization could be managed for efficiency. Overall, having all the “IT” people together was also considered efficient, and the general idea was that “the business” (sales, marketing, operations, and back-office functions like finance and human resources) would define their xref:KLP-system-intent["requirements"] and the IT organization would deliver systems in response. It was believed that organizing into "((centers of excellence))” (sometimes called _organizing by function_) would make the practices of each center more and more effective, and therefore more valuable to the organization as a whole. However, the new digital organizations perceived that there was too much friction between the different functions on the organization chart. Skepticism also started to emerge that “centers of excellence” were living up to their promise. Instead, what was too often seen was the emergence of an “us _versus_ them” mentality, as developers argued with the server and network engineers.

One of the first companies to try a completely different approach was ((Intuit)). As Intuit started selling its products increasingly as services, it re-organized and assigned individual infrastructure contributors - e.g., storage engineers and DBAs - to the product teams with which they worked cite:[Abbott2015], p.103.

[[fig-neworg-600-c]]
.New IT Organization
image::images/3_09-neworg.png[org chart,600]

[[spotify-model]]
(((Spotify)))((("Kniberg, Henrik")))((("Narayan, Sriram")))This model is also called the "((Spotify model))” (see <<fig-neworg-600-c>>). The dotted line boxes (Developers, Quality Assurance, Engineering) are no longer dedicated “centers of excellence” with executives leading them. Instead, they are lighter-weight “communities of interest” organized into chapters and guilds. The cross-functional product teams are the primary way work is organized and understood, and the communities of interest play a supporting role. Henrik Kniberg provided one of the first descriptions of how Spotify organizes along product lines cite:[Kniberg2012]. (Attentive readers will ask: “What happened to the PMO? And what about security?”. There are various answers to these questions, which we will continue to explore in Context III.)

One important note: Human resources management lines of authority in this model may still be by functional center, _not_ product line. This is the case at Spotify. However, the overall power structure shifts in favor of product focus decisively.

The consequences of this transition in organizational style are still being felt and debated. Sriram Narayan is in general an advocate of product organization. However, in his book _Agile Organization Design_, he points out that “IT work is labor-intensive and highly specialized”, and therefore managing IT talent is a particular organizational capability it may not make sense to distribute cite:[Narayam2015]. Furthermore, he observes that IT work is performed on medium to long time scales, and “IT culture” differs from “business culture”, concluding that "although a merger of business and IT is desirable, for the vast majority of big organizations it isn’t going to happen anytime soon".

((("Abbott, Martin")))((("Fisher, Michael")))Conversely, Abbott and Fisher in _The Art of Scalability_ argue that: "... The difference in mindset, organization, metrics, and approach between the IT and product models is vast. Corporate technology governance tends to significantly slow time-to-market for critical projects ... IT mindsets are great for internal technology development, but disastrous for external product development” cite:[Abbott2015(122-124)]. However, it is possible that Abbott and Fisher are overlooking the xref:trad-IT-decline[decline of traditional IT]. Hybrid models exist, with “product” teams reporting up under “business” executives, and the CIO still controlling the delivery staff who may be co-located with those teams. We will discuss the alternative models in more detail below.

[[KLP-conways-law]]
===== Conway's Law

anchor:conways-law[]Melvin Conway is a computer programmer who worked on early compilers and programming languages. In 1967 he proposed the thesis that:

_Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure._ cite:[Conway1968].

What does this mean? If we establish two teams, each team will build a piece of functionality (a feature or component). They will think in terms of “our stuff” and “their stuff” and the interactions (or _interface_) between the two. Perhaps this seems obvious, but as you scale up, it is critical to keep in mind. In particular, as you segment your organization along the xref:KLP-AKF-cube[AKF y-axis], you will need to keep in mind the difference between features and components. You are on a path to have dozens or hundreds of such teams. The decisions you make today on how to divide functionality and work will determine your operating model far into the future.

Ultimately, ((Conway's law)) tells us that to design a product is also to design an organization and _vice versa_. This is important for designers and architects to remember.

===== Defining the Organization

There are many different ways we can apply these ideas of traditional functional organizing _versus_ product-oriented organizing, and features _versus_ components. How do we begin to decide these questions? As a Digital Practitioner in a scaling organization, you need to be able to lead these conversations. The cross-functional, diverse, collaborative team is a key unit of value in the digital enterprise, and its performance needs to be nurtured and protected.

((("Abbott, Martin")))((("Fisher, Michael")))Abbott and Fisher suggest the following criteria when considering organizational structures cite:[Abbott2015(12)]:

• How easily can I add or remove people to/from this organization? Do I need to add them in groups, or can I add individual people?
• Does the organizational structure help or hinder the development of metrics that will help measure productivity?
• Does the organizational structure allow teams to own goals and feel empowered and capable of meeting them?
• Which types of conflict will arise, and will that conflict help or hinder the mission of the organization?
• How does this organizational structure help or hinder innovation within my company?
• Does this organizational structure help or hinder the time-to-market for my products?
• How does this organizational structure increase or decrease the cost per unit of value created?
• Does work flow easily through the organization, or is it easily contained within a portion of the organization?

===== Team Persistence

(((team persistence)))Team persistence is a key question. The practice in project-centric organizations has been temporary teams, that are created and broken down on an annual or more frequent basis. People “rolled on” and “rolled off” projects regularly in the common xref:heavyweight-pm[heavyweight project management](((heavyweight project management))) model. Often, contention for resources resulted in fractional project allocation, as in “you are 50% on Project A and 25% on Project B” which could be challenging for individual contributors to manage. With team members constantly coming and going, developing a deep, collective understanding of the work was difficult. Hard problems benefit from team stability. Teams develop both a deeper rational understanding of the problem, as well as emotional assets such as xref:KLP-Collaboration-basics[psychological safety](((psychological safety))). Both are disrupted when even one person on a team changes. Persistent teams of committed individuals also (in theory) reduce destructive xref:multi-tasking[multi-tasking and context-switching].

[[product-v-function]]
===== Product and Function

There is a fundamental tension between functional specialization and end-to-end value delivery — the tendency for specialist teams start to identify with their specialty and not the overall mission. The tension may go by different names:

* Product _versus_ function
* Value stream _versus_ activity
* Process _versus_ silo

As we saw xref:KLP-process-project-product[previously], there are three major concepts used to achieve an end-to-end flow across functional specialties:

* Product
* Project
* Process

These are not mutually-exclusive models and may interact with each other in complex ways. (See the scaling discussion in the xref:scaling-org[Context III introduction].)

===== Waterfall and Functional Organization

(((functional organization)))For example, some manufacturing can be represented as a very simple, sequential process model (see <<fig-naive-mfg-500-c>>).

[[fig-naive-mfg-500-c]]
.Simple Sequential Manufacturing
image::images/3_09-naive-mfg.png[manufacturing sequence,500]

The product is already defined, and the need to generate information (i.e., through xref:feedback[feedback]) is at an absolute minimum.

NOTE: Even in this simplest model, feedback is important. Much of the evolution of 20th century manufacturing has been in challenging this naive, ((open-loop)) model. (Remember our brief discussion of xref:open-loop[open-loop]?) The original, open-loop waterfall model of IT systems implementation (see <<fig-waterfallB-500-c>>) was arguably based on just such a naive concept.

[[fig-waterfallB-500-c]]
.Waterfall
image::images/3_09-waterfall.png[waterfall,500]

(Review xref:KLP-Agile-history[] on ((waterfall development)) and Agile history.) Functional, or _practice_, areas can continually increase their efficiency and economies of scale through deep specialization.

===== Defining “Practice"

(((practice, defined)))A “practice” is synonymous with “discipline” — it is a set of interrelated precepts, concerns, and techniques, often with a distinct professional identity. “Java programming”, “security”, or “capacity management” are practices. When an organization is closely identified with a practice, it tends to act as a functional silo (more on this to come). For example, in a traditional IT organization, the Java developers might be a separate team from the HTML, CSS and JavaScript specialists. The DBAs might have their own team, and also the architects, business analysts, and quality assurance groups. Each practice or functional group develops a strong professional identity as the custodians of “best practices” in their area. They may also develop a strong set of criteria for when they will accept work, which tends to slow down xref:KLP-lean-product-dev[product discovery].

There are two primary disadvantages to the model of projects flowing in a waterfall sequence across functional areas:

* It discourages closed-loop feedback
* There is transactional friction at each hand-off

Go back and review: the waterfall model falls into the “original sin” of IT management, xref:KLP-lean-product-dev[confusing production with product development]. As a repeatable production model, it may work, assuming that there is little or no information left to generate regarding the production process (an increasingly questionable assumption in and of itself). But when applied to ((product development)), where the *primary goal* is the experiment-driven generation of information, the model is inappropriate and has led to innumerable failures. This includes software development, and even implementing purchased packages in complex environments.

[[org-continuum]]
===== The Continuum of Organizational Forms

NOTE: The following discussion and accompanying set of diagrams is derived from Preston Smith and Don Reinertsen's thought regarding this problem in _Developing Products in Half the Time_ cite:[Smith1998] and _Managing the Design Factory_ cite:[Reinertsen1997]. Similar discussions are found in the _Guide to the Project Management Body of Knowledge_ cite:[PMI2013] and Abbott and Fisher's _The Art of Scalability_ cite:[Abbott2015].

((("Smith, Preston")))((("Reinertsen, Don")))((("Abbott, Martin")))((("Fisher, Michael")))There is a spectrum of alternatives in structuring organizations for flow across functional concerns. First, a lightweight “matrix” project structure may be implemented, in which the project manager has limited power to influence the activity-based work, where people sit, etc. (see <<fig-lightweight-pm-800-c>>).

[[fig-lightweight-pm-800-c]]
.Lightweight Project Management Across Functions
image::images/3_09-lightweight-pm.png[matrix figure, 800,]

Work flows across the ((functions)), perhaps called "((centers of excellence))”, and there may be contention for resources within each center. Often, simple “first in, first out” xref:queuing[queuing](((queuing))) approaches are used to manage the xref:ticketing[ticketed] work (((ticketing))), rather than more sophisticated approaches such as xref:cost-of-delay[cost of delay](((cost of delay))). It is the above model that Reinertsen was thinking of when he said: “The danger in using specialists lies in their low involvement in individual projects and the multitude of tasks competing for their time.” Traditional xref:tbl-i-o-matrix[I&O](((I&O))) organizations, when they implemented defined ((Service Catalog))s, can be seen as attempting this model. (More on this in the discussion of ITIL and xref:shared-services[shared services](((shared services))).)

[[heavyweight-pm]]
Second, a heavyweight project structure may specify much more, including dedicated time assignment, modes of work, standards, and so forth (see <<fig-heavy-pm-800-c>>). The vertical functional manager may be little more than a resource manager, but does still have reporting authority over the team member and crucially still writes their annual performance evaluation (if the organization still uses those). This has been the most frequent operating model in the xref:trad-cio-org[traditional CIO organization].

[[fig-heavy-pm-800-c]]
.Heavyweight Project Management Across Functions
image::images/3_09-heavy-pm.png[matrix figure, 800,]

If even more focus is needed — the now-minimized influence of the functional areas is still deemed too strong — the organization may move to completely product-based reporting (see <<fig-KLP-prod-mgmt-basics-800-c>>). With this, the team member reports to the product owner. There may still be communities of interest (Spotify guilds and tribes are good examples) (((Spotify))) and there still may be standards for technical choices.

[[fig-KLP-prod-mgmt-basics-800-c]]
.Product Team, Virtual Functions
image::images/3_09-product-mgmt.png[matrix figure, 800,]

[[skunkworks]]
Finally, in the ((skunkworks model)), all functional influence is deliberately blocked, as distracting or destructive to the product team's success (see <<fig-skunk-800-c>>).

[[fig-skunk-800-c]]
.Skunkworks Model
image::images/3_09-skunk.png[matrix figure, 800,]

The product team has complete autonomy and can move at great speed. It is also free to:

* Re-invent the wheel, developing new solutions to old and well-understood problems
* Bring in new components on a whim (regardless of whether they are truly necessary) adding to sourcing and long-term support complexity
* Ignore safety and security standards, resulting in risk and expensive retrofits

Early e-commerce sites were often set up as skunkworks to keep the interference of the traditional CIO to a minimum, and this was arguably necessary. However, ultimately, skunkworks is not scalable. Research by the ((Corporate Executive Board)) suggests that: “Once more than about 15% of projects go through the fast [skunkworks] team, productivity starts to fall away dramatically.” It also causes issues with morale, as a two-tier organization starts to emerge with elite and non-elite segments cite:[Goodwin2015].

Because of these issues, Don Reinertsen observes that: “Companies that experiment with autonomous teams learn their lessons, and conclude that the disadvantages are significant. Then they try to combine the advantages of the functional form with those of the autonomous team” cite:[Reinertsen1997].

The ((Agile movement)) is an important correction to dominant IT management approaches employing xref:open-loop[open-loop] delivery across centralized functional centers of excellence. However, the ultimate extreme of the skunkworks approach cannot be the basis for organization across the enterprise. While xref:product-v-function[functionally specialized organizations] have their challenges, they do promote understanding and common standards for technical areas. In a product-centric organization, communities of interest or practice provide an important counterbalancing platform for xref:KLP-coord-tools[coordination strategies] to maintain common understandings.

===== Scaling the Product Organization

((("Schwaber, Ken")))((("Cohn, Mike")))((("Larman, Craig")))((("Pichler, Roman"))) (((product owner)))(((Scrum master)))The functional organization scales well. Just keep hiring more Java programmers, or DBAs, or security engineers and assign them to xref:heavyweight-pm[projects] as needed. However, scaling product organizations requires more thought. The most advanced thinking in this area is found in the work of xref:KLP-scrum[Scrum] authors such as Ken Schwaber, Mike Cohn, Craig Larman, and Roman Pichler. Scrum, as we have discussed, is a strict, prescriptive framework calling for ((self-managing teams)) with:

* Product owner
* Scrum master
* Team member

[[fig-SoS-POs-500-o]]
.Product Owner Hierarchy
image::images/3_09-SoS-POs.png[hierarchy,500,,float="right"]

Let's accept Scrum and the xref:KLP-amazon-productization[2-pizza team] as our organizing approach. A large-scale Scrum effort is based on multiple small teams; e.g., representing xref:KLP-AKF-cube[AKF scaling cube partitions] (((AKF scaling cube))) (see <<fig-SoS-POs-500-o>>, similar to cite:[Pichler2010], p.12; cite:[Schwaber2007]). If we want to minimize xref:multi-tasking[multi-tasking and context-switching](((multi-tasking))), we need to ask: “How many product teams can a given product owner handle?”. In _Agile Product Management with Scrum_, Roman Pichler says: “My experience suggests that a product owner usually cannot look after more than two teams in a sustainable manner” cite:[Pichler2010(12)]. Scrum authors, therefore, suggest that larger-scale products be managed as aggregates of smaller teams. We will discuss how the product structure is defined in xref:chap-invest-mgmt[].

[[shared-services]]
===== From Functions to Components to Shared Services

We have previously discussed xref:feature-v-component[feature _versus_ component teams](((feature _versus_ component teams))). As a reminder, features are functional aspects of software (things people find directly valuable) while components are how software is organized (e.g., shared services and platforms such as data management).

As an organization grows, we see both the feature and component sides scale. Feature teams start to diverge into multiple products, while component teams continue to grow in the guise of shared services and platforms. Their concerns continue to differentiate, and communication friction may start to emerge between the teams. How an organization handles this is critical.

In a world of digital products delivered as services, both feature and component teams may be the recipients of ongoing investment. An ongoing objection in discussions of Agile is: “We can't put a specialist on every team!". This objection reflects the increasing depth of specialization seen in the evolving digital organization. Ultimately, it seems there are two alternatives to handling deep functional specialization in the modern digital organization:

* Split it across teams
* Turn it into an internal product

(((infrastructure engineering)))We have discussed the first option above (split the specialty across teams). But for the second option consider, for example, the traditional role of server engineer (a common infrastructure function). Such engineers historically have had a consultative, order-taking relationship to application teams:

* An application team would identify a need for computing capacity (“we need four servers”)
* The infrastructure engineers would get involved and provide recommendations on make, model, and capacity
* Physical servers would be acquired, perhaps after some debate and further approval cycles

Such processes might take months to complete, and often caused dissatisfaction. With the rise of ((cloud computing)), however, we see the transition from a consultative, order-taking model to an automated, always-on, ((self-service)) model. Infrastructure organizations move their activities from consulting on particular applications to designing and sustaining the shared, self-service platform. At that point, are they a function or a product?

===== Final Thoughts on Organization Forms

Formal organizational structures determine, to a great extent, how work gets done. But enterprise value requires that organizational units — whether product or functional — collaborate and coordinate effectively. Communications structures and interfaces, as covered in xref:KLP-chap-coordination[], are therefore an essential part of organizational design.

And of course, an empty structure is meaningless. You need to fill it with real people, which brings us to the topic of human resource management in the digital organization.

*Evidence of Notability*

Debates over organizational form are frequent of late. The years between 2010 and 2020 have seen a massive shift in many IT and digital organizations, from a focus on deep functional specialization to broader, cross-functional teams. Best practices and lessons at this writing are still unformed. Works such as Narayam's _Agile Organization Design_ cite:[Narayam2015] as well as coverage of the topic in other literature and industry guidance provide evidence of notability.

*Limitations*

Organization design influences, but does not completely determine, organizational results.


*Related Topics*

* xref:digital-context[Digital Context]
* xref:KLP-CA-product-mgmt[Product Management]
* xref:KLP-financial-mgmt[Financial Management]
* xref:KLP-sourcing[Sourcing]
* xref:KLP-structuring-investment[Portfolio Management]
* xref:KLP-people-mgmt[Human Resources Management]
* xref:KLP-culture[Culture]
* xref:gov-chap[Governance]
