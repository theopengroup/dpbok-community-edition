[[KLP-chap-coordination]]
=== Coordination and Process

*Area Description*

The digital team been executing its objectives since its earliest existence. Execution is whenever we meet demand with supply. (((demand management)))An idea for a new feature, to deliver some xref:KLP-digital-xform[digital value], is demand. The time spent implementing the feature is supply. The two combined is execution. Sometimes it goes well; sometimes it doesn't. Maintaining a tight xref:feedback[feedback] loop to continually assess execution is essential.

As the organization grows into multiple teams and multiple products, it has more complex execution problems, requiring coordination. The fundamental problem is the “D-word": _dependency_. Dependencies are why the organization must coordinate (work with no dependencies can scale nicely along the xref:KLP-AKF-cube[AKF x-axis]). But when there are dependencies (and there are various kinds) the organization needs a wider range of techniques. One xref:card-wall[Kanban board] is not sufficient to the task.

The practitioner must consider the xref:KLP-process-project-product[delivery models], as well (the “3 Ps": product, project, process, and now we have added program management). Decades of industry practice mean that people will tend to think in terms of these models and unless there is clarity about the nature of our work the organization can easily get pulled into non-value-adding arguments. To help understanding, this Competency Area will take a deeper look at process management, continuous improvement, and their challenges.
