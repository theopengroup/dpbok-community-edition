=== Context III Conclusion

Process management is a critical practice in business operations and will continue into the digital era. There are a variety of approaches, concerns, and techniques that must be considered and correctly applied.

xref:chap-invest-mgmt[] considered the fundamentals of project and resource management, with some caution and with an understanding that not all work is performed through classical project management and its Iron Triangle of cost, quality, and scope.

Finally, xref:chap-org-culture[] discussed organizational structure and culture, and the importance of culture in organizational effectiveness.

Context III is essential preparation for Context IV — essentially, Context III is about management, where Context IV expands into governance and the longest-lived, largest-scale concerns for digital delivery.

==== Context III Architectural View

[[fig-architectural-view3-800]]
.Architectural View
image::images/3_99-architectural-view.png[architectural view, 800]

As the organization scales to multiple products and teams, new components are required for coordinating and managing investment, provisioning multiple services, and abstracting operational issues when product to team cardinality is not 1:1. Formal ITSM queue-based processes (incident, change, problem) emerge here as mechanisms for communicating, executing, and assessing risk across team boundaries. Suggested functional components include:

•	Proposal Component
•	Portfolio Demand Component
•	Project Component
•	Service Design Component
•	Release Composition Component
•	Offer Management Component
•	Request Rationalization Component
•	Problem Component
•	Diagnostics & Remediation Component
•	Change Control Component
•	Incident Component
•	Event Component

NOTE: The requirements for many of these components are discussed in Context II, but their appearance in Context III indicates the increasing formalization and automation required to support team of teams activities.


// tag::learning-outcomes[]

*Context III "Architectural View" Learning Objectives*

* Identify the IT4IT components suitable for Context III

// end::learning-outcomes[]


*Related Topics*

* xref:KLP-structuring-investment[Portfolio Management]
* xref:KLP-project-mgmt[Project Management]
* xref:KLP-app-deliv[Application Delivery]
* xref:KLP-ops-mgmt[Operations Management]
* xref:KLP-ops-response[Operational Response]
* xref:KLP-monitoring[Monitoring and Telemetry]
