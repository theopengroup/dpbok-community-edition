[[KLP-work-management]]
=== Work Management

*Area Description*

(((work management))) When a team or a startup hires its first employees, and increases in size from two people to three or four, it is confronted with the fundamental issue of how work is tracked. The product team is now getting feedback from users calling for ((prioritization)), the allocation of resources, and the tracking of effort and completion. These are the critical day-to-day questions for any business larger than a couple of co-founders:

* What do we need to do?
* In what order?
* Who is doing it?
* Do they need help?
* Do they need direction?
* When will they be done?
* What do we mean by done?

People have different responsibilities and specialties, yet there is a common vision for delivering an IT-based product of some value. How is the work tracked towards this end? Perhaps the team is still primarily in the same location, but people sometimes are off-site or keeping different hours. Beyond product strategy, the team is getting support calls that result in fixes and new features. The initial signs of too much work-in-progress (slow delivery of final results, multi-tasking, and more) may be starting to appear.

The team has a product owner. They now institute xref:KLP-scrum[Scrum](((Scrum))) practices of a managed backlog, daily standups, and sprints. They may also use ((Kanban))-style task boards or card walls (to be described in this Competency Area), which are essential for things like support or other ((interrupt-driven work)). The relationship of these to your Scrum practices is a matter of ongoing debate. In general the team does not yet need full-blown project management (covered in Context III). The concept of "((ticketing))” will likely arise at this point. How this relates to your Scrum/Kanban approach is a question.

((("Reinertsen, Don")))Furthermore, while Agile principles and practices were covered in previous Competency Areas, there was limited discussion of *why* they work. This Competency Area covers Lean theory of product management that provides a basis for Agile practices; in particular, the work of Don Reinertsen.

The Competency Area title “Work Management” reflects earlier stages of organizational growth. At this point, neither formal project management, nor a fully realized process framework is needed, and the organization may not see a need to distinguish precisely between types of work processes. “It's all just work” at this stage.

