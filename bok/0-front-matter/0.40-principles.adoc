[[dpbok-principles]]
=== Principles of the DPBoK Standard

***
[NOTE]
====
This chapter is not part of the DPBoK Community Edition but is provided to preserve the chapter structure.

====
***

The DPBoK Standard defines a set of principles by which the document will evolve and be maintained, and how Digital Practitioner competencies will be defined.  These are described link:https://pubs.opengroup.org/dpbok/standard/DPBoK.html#dpbok-principles[here].

These include:

* Guiding Concepts
* Comprehensiveness
* Currency
* Capability-Based
* Verifiability
* Fine-grained and Clinical Terminology
* Compatibility with Other Frameworks
* Compatibility with Agile Principles
* Compatibility with Enterprise Architecture
* A Learning Artifact
* Developed as a Digital Product
* Competency-Based Content
* Scaling Model as Learning Progression
